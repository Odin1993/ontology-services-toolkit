package uio.ifi.ontology.toolkit.constraint.utils.pagoda_hermit;

import java.util.Collection;

import org.semanticweb.HermiT.model.AnnotatedEquality;
import org.semanticweb.HermiT.model.Atom;
import org.semanticweb.HermiT.model.AtomicRole;
import org.semanticweb.HermiT.model.DLClause;
import org.semanticweb.HermiT.model.DLPredicate;

import org.semanticweb.HermiT.model.Equality;
import org.semanticweb.HermiT.model.Inequality;

import org.semanticweb.HermiT.model.NodeIDLessEqualThan;
import org.semanticweb.HermiT.model.NodeIDsAscendingOrEqual;
import org.semanticweb.HermiT.model.Term;
import org.semanticweb.HermiT.model.Variable;

import uio.ifi.ontology.toolkit.constraint.model.clause.DatatypeAtom;
import uio.ifi.ontology.toolkit.constraint.model.clause.NegatedAtom;
import uio.ifi.ontology.toolkit.constraint.utils.Namespace;
import uio.ifi.ontology.toolkit.constraint.utils.Utility;


/**
 * Class addapted/reused from Pagoda to avoid dependency with the whole system and RDFox
 *
 */
public class RuleHelperExtended {	
	
	
	public static String getTextAndPrefixes(Collection<DLClause> clauses) {
		StringBuilder sb = new StringBuilder(getText(clauses)); 
		sb.insert(0, Utility.LINE_SEPARATOR); 
		sb.insert(0, MyPrefixes.PAGOdAPrefixes.prefixesText());
		return sb.toString(); 
	}
	
	
	
	public static String getText(Collection<DLClause> clauses) {		
		StringBuilder buf = new StringBuilder();
		for (DLClause cls: clauses) {
			buf.append(getText(cls));
			buf.append(Utility.LINE_SEPARATOR); 
		}
		return buf.toString(); 
	}
	
	
	
	

	
	public static String getText(DLClause clause) {
		StringBuffer buf = new StringBuffer();
		String atomText; 
		
		boolean lastSpace = true;
		for (Atom headAtom: clause.getHeadAtoms()) {
			if ((atomText = getText(headAtom)) == null) continue; 
			if (!lastSpace)	buf.append(" v "); 
			buf.append(atomText);
			lastSpace = false;
		}
		//TODO if not body then do not use :-    (added by ernesto, so that they can be printed as facts) 
		if (clause.getBodyAtoms().length>0)
			buf.append(" :- ");
		
		lastSpace = true;
		for (Atom bodyAtom: clause.getBodyAtoms()) {
//		for (String str: strs[1].split(", ")) {
			if ((atomText = getText(bodyAtom)) == null) continue; 
			if (!lastSpace) buf.append(", ");
			buf.append(atomText);
			lastSpace = false;
		}
		buf.append(" .");
		return buf.toString();
	}

	
	private static String getEqualityPredicate(){
		switch (Utility.datalog_engine) {
	 		case RDFox:
	 			return Namespace.EQUALITY_ABBR; 			
	 		case IRIS:
	 			// return "sameAs";
	 			//return Constants.IRIS_EQUAL;
	 			return Namespace.EQUALITY_NOPREFIX;
	     	default:
	     		return Namespace.EQUALITY_ABBR;
 			
		}
	}
	
	
	private static String getInequalityPredicate(){
		switch (Utility.datalog_engine) {
	 		case RDFox:
	 			return Namespace.INEQUALITY_ABBR; 			
	 		case IRIS:
	 			// return "differentFrom";
	 			//return Constants.IRIS_NOT_EQUAL;
	 			return Namespace.INEQUALITY_NOPREFIX;
	     	default:
	     		return Namespace.INEQUALITY_ABBR;
 			
		}
	}
	
	private static String getText(Atom atom) {
		if (atom.getDLPredicate() instanceof NodeIDsAscendingOrEqual ||
				atom.getDLPredicate() instanceof NodeIDLessEqualThan) 
			return null;
		
		StringBuilder builder = new StringBuilder(); 
		if (atom.getArity() == 1) {
			
			if (atom instanceof DatatypeAtom){ // && Parameters.RDFox_rules){ //adhoc addition by ernesto
				builder.append(atom.toString());
			}
			else{				
				if (atom instanceof NegatedAtom) //adhoc addition by ernesto
					builder.append("not ");
				builder.append(getText(atom.getDLPredicate())); 
				builder.append("("); 
				builder.append(getText(atom.getArgument(0)));
				builder.append(")");
			}
		}
		else {
			if (atom instanceof NegatedAtom)
				builder.append("not ");  //adhoc addition by ernesto.
			DLPredicate p = atom.getDLPredicate();
			if (p instanceof Equality || p instanceof AnnotatedEquality){
				
				builder.append(getEqualityPredicate());
				
			}
			else if (p instanceof Inequality){
				
				builder.append(getInequalityPredicate());
				
			}
			else builder.append(getText(p));
			builder.append("("); 
			builder.append(getText(atom.getArgument(0))); 
			builder.append(","); 
			builder.append(getText(atom.getArgument(1))); 
			builder.append(")"); 
		}
		return builder.toString(); 
	}
	
	public static String getText(DLPredicate p) {
		if (p instanceof Equality || p instanceof AnnotatedEquality) {
			
			return getEqualityPredicate();
			
		}
		if (p instanceof Inequality){
			
			return getInequalityPredicate();
			
		}
		if (p instanceof AtomicRole && ((AtomicRole) p).getIRI().startsWith("?")){					
			return ((AtomicRole) p).getIRI(); 
		}
		
		
		String abbreviated_string = MyPrefixes.PAGOdAPrefixes.abbreviateIRI(p.toString());
		
		
		//Predicate names		
		switch (Utility.datalog_engine) {
	 		case RDFox:
	 			return abbreviated_string;
	 		case IRIS:
	 			if (!abbreviated_string.contains("http://") && abbreviated_string.indexOf(":")>0){
					String abbreviated_string_iris = abbreviated_string.substring(abbreviated_string.indexOf(":")+1, abbreviated_string.length());
					//System.out.println(p.toString() + " -> " + abbreviated_string_iris);
					return abbreviated_string_iris;
				}
				else{
					return abbreviated_string;
				}	 			     	
	     	default:
	     		return abbreviated_string;
				
		}
		
		
	}

	public static String getText(Term t) {
		if (t instanceof Variable)
			return "?" + ((Variable) t).getName(); 
		
						
		String abbreviated_string = MyPrefixes.PAGOdAPrefixes.abbreviateIRI(t.toString());
		//System.out.println(t.toString() + " -> " + abbreviated_string);
				
		
		//Variables, constants and URIS
		switch (Utility.datalog_engine) {
	 		case RDFox:
	 			return abbreviated_string;
	 		case IRIS:
	 			//For Individuals
				if (t.toString().startsWith("<http://")){
					//keep URI and quote				
					return "'" + Utility.removeAngles(t.toString()) + "'";
				}
				else if (t.toString().indexOf("^^")>0){
					//remove datatype from constants
					return t.toString().substring(0, t.toString().indexOf("^^"));
				}
				else{
					return abbreviated_string;
				}
	     	default:
	     		return abbreviated_string;
				
		}
		
	}

}
