/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package uio.ifi.ontology.toolkit.projection.controller.triplestore;

/**
 * This class stores the graph projection into an RDF4J triple store and will delegate the management 
 * to the used RDF end point   
 * Uses RDF4J as manager of the projection + inference engine?
 * @author ernesto
 * Created on 8 Nov 2017
 *
 */
public class RDF4JProjectionManager {

	//TODO 
	//Similar to RDFox but without propagation unless it is plugged to an inference engine...
	//Limited basic implementation
	
	
}
