1. Test basic visualization

2. Test top-down propagation of property neighbours and facets

3. Test bottom-up propagation of property neighbours and facets

4. Test top-bottom propagation of ranges for object properties

5. Test propagation of domain and range (behaviour of Top and several domains/ranges)

6. Test datatypes in facets

7. Test range sliders (and multiple cases)

8. Test enumeration of values (and multiple cases)

9. Test Optique annotations and lexical annotations

10. Real cases: well_ontology, NPD
